import React from "react";

export default function ModalComfirm({ movieInfor, tickets, onSuccess }) {
  let { tenCumRap, tenPhim, tenRap, diaChi, hinhAnh, ngayChieu, gioChieu } =
    movieInfor;
  let handleMoney = () => {
    let tongTien = 0;
    tickets.map((item) => {
      tongTien += item.giaVe * 1;
    });
    return tongTien.toLocaleString();
  };
  return (
    <div className="p-5 h-4/5">
      <p className="text-black text-3xl border-b border-slate-500 mb-6">
        {tenPhim}
      </p>
      <div className="flex flex-col justify-between h-4/5">
        <div className="text-black text-lg mb-3">
          <p>
            <span className="mr-2 text-sky-500">
              <i class="fa fa-home"></i>
            </span>
            Tên cụm rạp:
          </p>
          <p className="text-slate-500 text-sm italic">{tenCumRap}</p>
        </div>
        <p className="text-black text-lg mb-3 pb-3 border-b border-slate-500">
          <p>
            <span className="mr-2 text-sky-500">
              <i class="fa fa-map-marker-alt"></i>
            </span>
            Địa chỉ:
          </p>
          <p className="text-slate-500 text-sm italic">{diaChi}</p>
        </p>
        <div className="text-black text-lg mb-3">
          <p>
            <span className="mr-2 text-sky-500">
              <i class="fa fa-calendar-alt"></i>
            </span>
            Ngày chiếu:
          </p>
          <p className="text-slate-500 text-sm italic">{ngayChieu}</p>
        </div>
        <div className="text-black text-lg mb-3">
          <p>
            <span className="mr-2 text-sky-500">
              <i class="fa fa-clock"></i>
            </span>
            Giờ chiếu:
          </p>
          <p className="text-slate-500 text-sm italic">{gioChieu}</p>
        </div>
        <div className="text-black text-lg mb-3">
          <p>
            <span className="mr-2 text-sky-500">
              <i class="fa fa-door-open"></i>
            </span>
            Tên rạp
          </p>
          <p className="text-slate-500 text-sm italic">{tenRap}</p>
        </div>
        <div className="text-black text-lg mb-3 pb-3 border-b border-slate-500">
          <p>
            <span className="mr-2 text-sky-500">
              <i class="fa fa-couch"></i>
            </span>
            Ghế:
          </p>
          <div className="text-slate-500 text-sm italic">
            {tickets.map((item) => {
              return (
                <span
                  className="text-sky-500 bg-slate-200 rounded-xl px-2 py-1 mr-1"
                  key={item.maGhe}
                >
                  [-{item.tenGhe}-].
                </span>
              );
            })}
          </div>
        </div>
        <div className="text-black text-xl font-medium flex justify-between items-start">
          <span>Tổng tiền:</span>
          <span className="w-3/5 text-right text-black text-xl font-bold">
            {handleMoney()}VND
          </span>
        </div>
        <button
          onClick={onSuccess}
          className="block text-3xl text-white font-bold bg-sky-500 py-4 px-8 mt-4 rounded-3xl hover:bg-sky-300 hover:shadow-lg hover:shadow-sky-400 transition duration-300"
        >
          Thanh toán
        </button>
      </div>
    </div>
  );
}
