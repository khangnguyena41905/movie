import React from "react";
import { useRef } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { theaterServ } from "../../service/theaters.service";
import DetailBanner from "./DetailBanner";
import TabMovie from "./TabMovie";

export default function DetailPage() {
  let tabMovieRef = useRef();
  let { id } = useParams();
  const scrollToTabMovie = () => {
    tabMovieRef.current.scrollIntoView({ behavior: "smooth" });
  };
  let [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    console.log("id: ", id);

    theaterServ
      .getMovieSchedule(id)
      .then((res) => {
        console.log("res: ", res.data.content);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }, []);
  return (
    <div className="w-full pb-20">
      <DetailBanner data={dataMovie} scrollToTabMovie={scrollToTabMovie} />
      <div
        ref={tabMovieRef}
        className="md:w-3/5 md:mx-auto md:mt-10 w-full mt-6"
      >
        <TabMovie data={dataMovie} />
      </div>
    </div>
  );
}
