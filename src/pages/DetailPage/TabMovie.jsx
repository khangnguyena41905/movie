import React, { useState } from "react";
import { Button, Modal, Tabs } from "antd";
import Lottie from "lottie-react";
import wating from "../../asset/animation/waiting.json";
import { userInfoLocal } from "../../service/local.service";
import ModalLogin from "../Homepage/MovieTabs/ModalLogin";
export default function TabMovie(props) {
  let { heThongRapChieu } = props.data;
  console.log("heThongRapChieu: ", heThongRapChieu);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      {heThongRapChieu && heThongRapChieu.length == true ? (
        <div className="w-full">
          <Tabs defaultActiveKey="0">
            {heThongRapChieu?.map((heThongRap) => {
              return (
                <Tabs.TabPane
                  tab={<img className="w-20" src={heThongRap.logo} alt="" />}
                  key={heThongRap.maHeThongRap}
                >
                  <Tabs defaultActiveKey="0">
                    {heThongRap.cumRapChieu?.map((cumRap) => {
                      return (
                        <Tabs.TabPane
                          tab={<p>{cumRap.tenCumRap}</p>}
                          key={cumRap.maCumRap}
                        >
                          <div className="w-full text-left">
                            {cumRap.lichChieuPhim?.map((lichChieu) => {
                              return (
                                <Button
                                  key={lichChieu.maLichChieu}
                                  onClick={() => {
                                    let userLocal = userInfoLocal.get();
                                    if (userLocal) {
                                      window.open(
                                        `/booking/${lichChieu.maLichChieu}`
                                      );
                                    } else {
                                      showModal();
                                    }
                                  }}
                                  danger
                                  type="primary"
                                >
                                  {lichChieu.ngayChieuGioChieu}
                                  <span>
                                    <Modal
                                      open={isModalOpen}
                                      footer={null}
                                      onCancel={handleCancel}
                                    >
                                      <ModalLogin handleOk={handleOk} />

                                      <div className="w-3/5 mx-auto flex justify-between items-center">
                                        <div className="w-1/3 text-center">
                                          <span className="text-5xl text-blue-800">
                                            <i class="fab fa-facebook-square"></i>
                                          </span>
                                        </div>
                                        <div className="w-1/3 text-center">
                                          <span className="text-5xl text-green-600">
                                            <i class="fab fa-google-plus-square"></i>
                                          </span>
                                        </div>
                                        <div className="w-1/3 text-center">
                                          <span className="text-5xl text-blue-500">
                                            <i class="fab fa-twitter-square"></i>
                                          </span>
                                        </div>
                                      </div>

                                      <Button
                                        block
                                        type="primary"
                                        className="mt-3 bg-indigo-500 hover:shadow-lg hover:shadow-indigo-500/50 transition duration-300"
                                        onClick={() => {
                                          window.open("/register");
                                        }}
                                      >
                                        Đăng ký
                                      </Button>
                                    </Modal>
                                  </span>
                                </Button>
                              );
                            })}
                          </div>
                        </Tabs.TabPane>
                      );
                    })}
                  </Tabs>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </div>
      ) : (
        <div className="w-full container">
          <h2 className=" text-5xl font-serif text-red-600">
            Hiện tại chưa có lịch chiếu
            <span>
              <i class="fa fa-sad-tear"></i>
            </span>
          </h2>
          <div className="w-3/5 mx-auto">
            <Lottie animationData={wating} />
          </div>
        </div>
      )}
    </>
  );
}
