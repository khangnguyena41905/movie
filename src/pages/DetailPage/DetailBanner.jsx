import React, { useState } from "react";
import Header from "../../component/Header";
import { Rate } from "antd";
import { useRef } from "react";
import ModalMovie from "../Homepage/MovieList/ModalMovie";
export default function DetailBanner(props) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  let { tenPhim, hinhAnh, trailer, danhGia, moTa } = props.data;
  let scrollToTabMovie = props.scrollToTabMovie;
  return (
    <div>
      <Header />
      <div
        style={{
          backgroundImage: `url(${hinhAnh})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
        }}
        className="w-full"
      >
        <div
          style={{
            background: "rgba( 0, 0, 0, 0.25 )",
            boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
            backdropFilter: "blur( 15px )",
            WebkitBackdropFilter: " blur( 15px )",
            borderRadius: "10px",
            border: "1px solid rgba( 255, 255, 255, 0.18 )",
          }}
          className="w-full py-20 md:flex md:h-90"
        >
          <div className="w-full md:w-1/3">
            <div className="flex justify-center w-full md:block md:h-full relative">
              <img
                className="h-80 md:absolute md:bottom-0 md:right-0"
                src={hinhAnh}
                alt=""
              />
            </div>
          </div>
          <div className="md:w-2/3 relative flex flex-col justify-evenly items-start px-8">
            <div className="text-left mt-4">
              <p className="md:text-6xl text-2xl text-white font-mono">
                {tenPhim}
              </p>
              <div>
                <Rate disabled value={danhGia * 0.5} allowHalf />
              </div>
              <p className="md:text-lg md:mt-6 mt-2 text-sm text-gray-300">
                Mô tả: {moTa}
              </p>
            </div>
            <div className="flex w-full mt-4">
              <div className="md:w-1/4 w-1/2 text-left">
                <button
                  onClick={() => {
                    scrollToTabMovie();
                  }}
                  className="text-white block md:w-3/4 w-3/4 py-4 bg-gradient-to-r from-rose-600 to-purple-600 hover:from-rose-500 hover:to-purple-500"
                >
                  Đặt vé ngay
                </button>
              </div>
              <div className="md:w-1/4 w-1/2 text-left">
                <button
                  onClick={showModal}
                  className="text-white block md:w-3/4 w-3/4 py-4 bg-gradient-to-r from-rose-600 to-purple-600 hover:from-rose-500 hover:to-purple-500"
                >
                  Trailer
                </button>
                <ModalMovie
                  isModalOpen={isModalOpen}
                  handleCancel={handleCancel}
                  video={trailer}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
