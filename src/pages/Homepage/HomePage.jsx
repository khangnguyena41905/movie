import React from "react";
import { useRef } from "react";
import Header from "../../component/Header";
import Application from "./Application/Application";
import BookingTicketBar from "./BookingTicketBar/BookingTicketBar";
import Footer from "./Footer/Footer";
import HomePageCarousel from "./HomePageCarousel/HomePageCarousel";
import MovieCarousel from "./MovieList/MovieCarousel";
import MovieSlide from "./MovieList/MovieSlide";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  const movieCarouselRef = useRef();
  const movieTabRef = useRef();
  const appRef = useRef();
  const scrollToCarousel = () => {
    movieCarouselRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const scrollToMovieTab = () => {
    movieTabRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const scrollToAppRef = () => {
    appRef.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div className="bg-black">
      <Header
        lichChieu={scrollToCarousel}
        cumRap={scrollToMovieTab}
        ungDung={scrollToAppRef}
      ></Header>
      <HomePageCarousel />
      <BookingTicketBar />
      {/* <MovieSlide /> */}
      <div ref={movieCarouselRef}>
        <MovieCarousel />
      </div>
      <div ref={movieTabRef}>
        <MovieTabs />
      </div>
      <div ref={appRef}>
        <Application />
      </div>
      <Footer />
    </div>
  );
}
