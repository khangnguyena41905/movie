import { Modal } from "antd";
import React from "react";
import { useState } from "react";
import ReactPlayer from "react-player";

export default function ModalMovie({ isModalOpen, handleCancel, video }) {
  return (
    <>
      {isModalOpen ? (
        <Modal
          open={isModalOpen}
          onCancel={handleCancel}
          width={1000}
          footer={null}
          bodyStyle={{ background: "gray" }}
        >
          <ReactPlayer width="100%" style={{ marginTop: "3%" }} url={video} />
        </Modal>
      ) : null}
    </>
  );
}
