import React from "react";
import MovieCarouselList from "./MovieCarouselList";

export default function MovieCarousel() {
  return (
    <div className="container text-white my-10 mx-auto">
      <div className="mt-10">
        <h2 className="text-white text-4xl text-left mb-4 pl-3">Phim mới</h2>
        <MovieCarouselList maNhom={"GP01"} />
      </div>
      <div className="mt-10">
        <h2 className="text-white text-4xl text-left mb-4 pl-3">
          Top thịnh hành
        </h2>
        <MovieCarouselList maNhom={"GP02"} />
      </div>
      <div className="mt-10">
        <h2 className="text-white text-4xl text-left mb-4 pl-3">Chiếu rạp</h2>
        <MovieCarouselList maNhom={"GP03"} />
      </div>
    </div>
  );
}
