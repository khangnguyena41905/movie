import React from "react";
import Slider from "react-slick";
import { RightOutlined, LeftOutlined } from "@ant-design/icons";

import { useState } from "react";
import { useEffect } from "react";
import { movieServ } from "../../../service/movies.service";
import MovieItem from "./MovieItem";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useRef } from "react";

export default function MovieCarouselList({ maNhom }) {
  let [movieItems, setMovieItems] = useState([]);
  let movieItemRef = useRef();
  const netArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "red" }}
        onClick={onClick}
      />
    );
  };
  let settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  let fetchMovieList = async (maNhom) => {
    try {
      let params = {
        maNhom,
        soTrang: 1,
        soPhanTuTrenTrang: 12,
      };
      let res = await movieServ.getMovieLists({ params });
      setMovieItems(res.data.content.items);
    } catch (error) {
      console.log("error");
    }
  };
  useEffect(() => {
    fetchMovieList(maNhom);
  }, []);
  return (
    <div className="w-full relative">
      <div>
        <Slider ref={movieItemRef} {...settings}>
          {movieItems?.map((item) => {
            return (
              <div className="px-3" key={item.maPhim}>
                <MovieItem items={item} />
              </div>
            );
          })}
        </Slider>
      </div>
      <div
        className="absolute top-1/2 left-0 md:-translate-x-full -translate-y-1/2 cursor-pointer"
        onClick={() => {
          movieItemRef.current.slickPrev();
        }}
      >
        <LeftOutlined
          style={{ fontSize: "3rem" }}
          className="transition ease-in-out text-stone-300 hover:text-rose-700 hover:scale-150 duration-300"
        />
      </div>
      <div
        className="absolute top-1/2 right-0 md:translate-x-full -translate-y-1/2 cursor-pointer"
        onClick={() => {
          movieItemRef.current.slickNext();
        }}
      >
        <RightOutlined
          style={{ fontSize: "3rem" }}
          className="transition ease-in-out text-stone-300 hover:text-rose-700 hover:scale-150 duration-300"
        />
      </div>
    </div>
  );
}
