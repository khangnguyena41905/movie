import { Button, Checkbox, Form, Input, message } from "antd";
import Lottie from "lottie-react";
import login_modal from "../../../asset/animation/login-modal.json";
import React from "react";
import { setLoginActionService } from "../../../redux/actions/userActions";
import { useDispatch } from "react-redux";

export default function ModalLogin({ handleOk }) {
  let dispatch = useDispatch();
  const onFinish = (values) => {
    dispatch(setLoginActionService(values, handleOk));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      <div className="h-60">
        <Lottie className="h-full " animationData={login_modal} />
      </div>
      <Form
        name="basic"
        layout="vertical"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item className="text-center">
          <Button
            type="primary"
            className="border-0 bg-cyan-500 hover:bg-cyan-300 hover:shadow-lg hover:shadow-cyan-500/50 transition duration-300"
            htmlType="submit"
          >
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
