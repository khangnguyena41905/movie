import { Button, Modal } from "antd";
import moment from "moment";
import React, { useState } from "react";
import { userInfoLocal } from "../../../service/local.service";
import ModalLogin from "./ModalLogin";

export default function MovieTabItem({ movie }) {
  let { tenPhim, lstLichChieuTheoPhim, hinhAnh } = movie;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className="flex justify-center items-center w-full p-3">
      <div className="w-1/3 h-60 flex items-center overflow-hidden">
        <img className="w-full object-cover" src={hinhAnh} alt="" />
      </div>
      <div className="w-2/3 h-60 text-left">
        <p className="text-2xl text-indigo-300 ml-1 mb-2">
          {" "}
          <span className="bg-gradient-to-r from-indigo-500 via-pink-500 to-rose-500 text-white rounded-xl px-2 py-1">
            C18
          </span>{" "}
          {tenPhim}
        </p>
        <div className="h-1/2 overflow-auto">
          {lstLichChieuTheoPhim.slice(0, 5).map((lichChieu) => {
            return (
              <span key={lichChieu.maLichChieu}>
                <Button
                  type="text"
                  onClick={() => {
                    let userLocal = userInfoLocal.get();
                    if (userLocal) {
                      window.open(`/booking/${lichChieu.maLichChieu}`);
                    } else {
                      showModal();
                    }
                  }}
                  className="mt-1 mx-1 bg-transparent text-white border-0 hover:bg-gradient-to-r hover:from-rose-400 hover:to-blue-500 hover:text-white transition duration-300 ease-in-out"
                >
                  {moment(lichChieu.ngayChieuGioChieu).format(
                    "DD/MM/YYYY - HH:MM"
                  )}
                </Button>
                <span>
                  <Modal
                    open={isModalOpen}
                    footer={null}
                    onCancel={handleCancel}
                  >
                    <ModalLogin handleOk={handleOk} />

                    <div className="w-3/5 mx-auto flex justify-between items-center">
                      <div className="w-1/3 text-center">
                        <span className="text-5xl text-blue-800">
                          <i class="fab fa-facebook-square"></i>
                        </span>
                      </div>
                      <div className="w-1/3 text-center">
                        <span className="text-5xl text-green-600">
                          <i class="fab fa-google-plus-square"></i>
                        </span>
                      </div>
                      <div className="w-1/3 text-center">
                        <span className="text-5xl text-blue-500">
                          <i class="fab fa-twitter-square"></i>
                        </span>
                      </div>
                    </div>

                    <Button
                      block
                      type="primary"
                      className="mt-3 bg-indigo-500 hover:shadow-lg hover:shadow-indigo-500/50 transition duration-300"
                      onClick={() => {
                        window.open("/register");
                      }}
                    >
                      Đăng ký
                    </Button>
                  </Modal>
                </span>
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
