import { combineReducers } from "redux";
import { UserReducer } from "./UserReducer";
import { TicketRoomReducer } from "./TicketRoomReducer";
import { loadingReducer } from "./loadingReducer";
export let rootReducer = combineReducers({
  UserReducer,
  TicketRoomReducer,
  loadingReducer,
});
