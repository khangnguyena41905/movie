import React from "react";
import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import UserNav from "./UserNav";
import { useMediaQuery } from "react-responsive";
import UserNavPhone from "./UserNavPhone";
import { useNavigate } from "react-router-dom";

export default function Header({ lichChieu, cumRap, ungDung }) {
  const navigate = useNavigate();
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 770px)",
  });
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 770px)" });
  return (
    <Popover className="fixed z-10 w-full bg-black bg-opacity-60">
      <div className="mx-auto max-w-7xl px-4 sm:px-6">
        <div className="flex items-center justify-between py-2 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-0 lg:flex-1">
            <a
              onClick={() => {
                navigate("/");
                window.scrollTo({ top: 0, behavior: "smooth" });
              }}
            >
              <span className="sr-only">Your Company</span>
              <img
                className="h-8 w-auto sm:h-16"
                src="https://as1.ftcdn.net/v2/jpg/05/38/46/58/1000_F_538465893_XSGEFHuNnFJ9meR00KmzGkO9XEK3hriU.webp"
                alt=""
              />
            </a>
          </div>
          <div className="-my-2 -mr-2 md:hidden">
            <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Open menu</span>
              <Bars3Icon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
            {/* start dropdown menu */}
            <Transition
              as={Fragment}
              enter="duration-200 ease-out"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="duration-100 ease-in"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Popover.Panel
                focus
                className="absolute inset-x-0 top-0 origin-top-right transform p-2 transition md:hidden"
              >
                <div className="divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
                  <div className="px-5 pt-5 pb-6">
                    <div className="flex items-center justify-between">
                      <div
                        onClick={() => {
                          navigate("/");
                          window.scrollTo({ top: 0, behavior: "smooth" });
                        }}
                      >
                        <img
                          className="h-8 w-auto"
                          src="https://as1.ftcdn.net/v2/jpg/05/38/46/58/1000_F_538465893_XSGEFHuNnFJ9meR00KmzGkO9XEK3hriU.webp"
                          alt="Your Company"
                        />
                      </div>
                      <div className="-mr-2">
                        <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                          <span className="sr-only">Close menu</span>
                          <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                        </Popover.Button>
                      </div>
                    </div>
                    <div className="mt-6 flex flex-col justify-between items-center h-36">
                      <a
                        onClick={() => {
                          lichChieu();
                        }}
                        className="text-base font-medium text-gray-500 hover:text-rose-700"
                      >
                        Lịch chiếu
                      </a>

                      <a
                        onClick={() => {
                          cumRap();
                        }}
                        className="text-base font-medium text-gray-500 hover:text-rose-700"
                      >
                        Cụm rạp
                      </a>
                      <a
                        href="https://www.youtube.com/c/Ph%C3%AAPhim/community"
                        className="text-base font-medium text-gray-500 hover:text-rose-700"
                      >
                        Tin tức
                      </a>
                      <a
                        onClick={() => {
                          ungDung();
                        }}
                        className="text-base font-medium text-gray-500 hover:text-rose-700"
                      >
                        Ứng dụng
                      </a>
                    </div>
                  </div>
                  <div className="space-y-6 py-6 px-5 w-full">
                    <div className="w-full">
                      {isTabletOrMobile && <UserNavPhone />}
                    </div>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
            {/* end dropdown menu */}
          </div>
          <Popover.Group as="nav" className="hidden space-x-10 md:flex">
            <a
              onClick={() => {
                lichChieu();
              }}
              className="text-base font-medium text-gray-500 hover:text-rose-700"
            >
              Lịch chiếu
            </a>

            <a
              onClick={() => {
                cumRap();
              }}
              className="text-base font-medium text-gray-500 hover:text-rose-700"
            >
              Cụm rạp
            </a>
            <a
              href="https://www.youtube.com/c/Ph%C3%AAPhim/community"
              className="text-base font-medium text-gray-500 hover:text-rose-700"
            >
              Tin tức
            </a>
            <a
              onClick={() => {
                ungDung();
              }}
              className="text-base font-medium text-gray-500 hover:text-rose-700"
            >
              Ứng dụng
            </a>
          </Popover.Group>
          {isDesktopOrLaptop && <UserNav />}
        </div>
      </div>
    </Popover>
  );
}
Header.defaultProps = {
  lichChieu: () => {
    window.open("/", "_self");
  },
  cumRap: () => {
    window.open("/", "_self");
  },
  ungDung: () => {
    window.open("/", "_self");
  },
};
