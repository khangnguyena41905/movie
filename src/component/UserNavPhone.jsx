import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { SET_LOGIN } from "../redux/constants/userConstant";
import { userInfoLocal } from "../service/local.service";

export default function UserNavPhone() {
  let dispath = useDispatch();
  let userInfor = useSelector((state) => {
    return state.UserReducer.userInfor;
  });
  let handleLogout = () => {
    userInfoLocal.remove();
    dispath({
      type: SET_LOGIN,
      payload: null,
    });
  };
  let renderContent = () => {
    if (userInfor) {
      return (
        <div className="">
          <div className="whitespace-nowrap text-base font-medium text-gray-500 hover:text-rose-700">
            {userInfor.hoTen}
          </div>
          <button
            onClick={handleLogout}
            className="inline-flex items-center justify-center whitespace-nowrap rounded-md border border-transparent bg-rose-700 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-rose-800"
          >
            Log out{" "}
          </button>
        </div>
      );
    } else {
      return (
        <div className="">
          <div>
            <NavLink
              to="/login"
              className="block text-base font-medium text-white bg-gradient-to-r from-rose-500 to-purple-500 px-4 py-2 shadow-sm rounded-md"
            >
              Sign in
            </NavLink>
          </div>
          <div>
            <span>You are not member? </span>
            <NavLink
              to="/register"
              className="inline-block text-base font-medium text-gray-400"
            >
              Sign up
            </NavLink>
          </div>
        </div>
      );
    }
  };
  return <>{renderContent()}</>;
}
