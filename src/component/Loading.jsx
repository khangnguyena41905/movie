import React from "react";
import Lottie from "lottie-react";
import loading_animation from "../asset/animation/loading_animation.json";
import { useSelector } from "react-redux";
export default function Loading() {
  let isLoading = useSelector((state) => {
    return state.loadingReducer.isLoading;
  });
  return (
    <>
      {isLoading ? (
        <div
          style={{
            zIndex: "99999",
            background: "rgba( 255, 255, 255, 0.15 )",
            boxShadow: "0 8px 32px 0 rgba( 31, 38, 135, 0.37 )",
            backdropFilter: "blur( 3.5px )",
            WebkitBackdropFilter: "blur( 3.5px )",
            borderRadius: "10px",
            border: "1px solid rgba( 255, 255, 255, 0.18 )",
          }}
          className="fixed w-screen h-screen flex justify-center items-center"
        >
          <div>
            <Lottie animationData={loading_animation} />
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
}
